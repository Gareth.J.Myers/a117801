// produce triangle numbers, check they match the form:
// all digits except one digit are 1. The digit that isn't a 1 must be 2, 3, 5, or 7.
// uses unsigned long long so can calulate up to the 6074000999th triangle number (1.844674e+19)
// That should take around 2 hours to do?
#include <iostream>
#include <string>

// generate the nth triangle number
unsigned long long nth_tri(unsigned long long n) {
	unsigned long long result = (n * (n - 1)) / 2;
	return result;
}

// given a triangle number, check if its digit product is prime (see line 2)
bool prime_prod(unsigned long long trinum) {
	std::string tri_str = std::to_string(trinum);
	int str_length = tri_str.length();
	int num_primes = 0;
	bool any_nonprimes = false;
	
	for (int i = 0; i < str_length; i++) {
		int char_int = ((int) tri_str[i]) - ((int) '0');
		if (char_int != 1) {
			if (char_int == 2 || char_int == 3 || char_int == 5 || char_int == 7) {
				num_primes++;
			} else {
				any_nonprimes = true;
			}
		} else {
		}
	}
	
	if (any_nonprimes || num_primes > 1) {
		return false;
	} else {
		return true;
	}
}

// given a number, check if product of digits of nth triangle num is prime
bool check_nth_tri(unsigned long long n) {
	unsigned long long tri = nth_tri(n);
	return prime_prod(tri);
}

// given a start number and stop number, test all the nth triangle
// numbers in that range. Output to terminal ones where the product of the
// digits is prime.
void check_range(unsigned long long start_num, unsigned long long end_num) {
	for (unsigned long long i = start_num; i <= end_num; i++) {
		
		if (i % 1000000 == 0) {
			std::cout << "On " << i << "/6074000998\r";
		}
		
		if (check_nth_tri(i)) {
			std::cout << "\n" << i << ": " << nth_tri(i) << "          \n";
		}
	}
}

// DEBUG
int main() {
	check_range(1, 6074000998);
	return 0;
}