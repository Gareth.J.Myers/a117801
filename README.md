# A117801

Finding terms of the OEIS sequence A117801 "Triangular numbers for which the product of the digits is a prime number".

Comment #17 on http://oeis.org/history?seq=A117801 claims that there are no terms _t_ for 1711 < _t_ < 10^2580.

This code shows that there are no terms for 1711 < _t_ < 6074000998.

Which is less but I just wanted to write some code for practice.